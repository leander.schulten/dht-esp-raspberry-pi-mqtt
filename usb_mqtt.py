#!/usr/bin/env python3
import time
import serial
import paho.mqtt.client as mqtt
MQTT_TOPIC = "test"
SERIAL_PORT = "" # leave empty to autodetect 
serial1 = serial.Serial()
serial1.baudrate = 57600
serial1.timeout = 30

client = mqtt.Client()
client.will_set(MQTT_TOPIC + "/online", payload=0, qos=0, retain=True)
client.connect("hilton.rwth-aachen.de", 1883, 30)
client.loop_start()

if not SERIAL_PORT:
	import serial.tools.list_ports
else:
	serial1.port  = SERIAL_PORT

try:
	while True:
		if not SERIAL_PORT and not serial1.is_open:
			for port in serial.tools.list_ports.comports():
				if "USB" in port.device or "COM" in port.device:
					serial1.port  = port.device

		if serial1.port :
			serial1.open()

		while serial1.is_open:
			lineRaw = serial1.readline()
			line = lineRaw.decode("utf8").strip()
			if not line:
				continue
			array = line.split(':')
			if array[1] is "NaN":
				client.publish(MQTT_TOPIC + '/error', 1, 1)	
			else:
				client.publish(MQTT_TOPIC + '/' + array[0], array[1], 1)
				client.publish(MQTT_TOPIC + '/error', 0, 1)	
			client.publish(MQTT_TOPIC + '/online', 1, 1)

		client.publish(MQTT_TOPIC + '/online', 0, 1)
		time.sleep(5.0)
except KeyboardInterrupt:
    pass

client.publish(MQTT_TOPIC + '/online', 0, 1)
client.loop_stop()
client.disconnect()
