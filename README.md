## Hardware
- Raspberry Pi
- USB Datenkabel 
- ESP 8266 D1 Mini 
- DHT 11 Temperatur Sensor

## ESP Software
Arduino Code ist hier im Git.  

Die Boardinformationen für den ESP8266 müssen in Arduino IDE hinzuzufügen werden, dafür die Einstellungen öffnen (_File_ -> _Preferences_) und die URL `http://arduino.esp8266.com/stable/package_esp8266com_index.json` in das Feld _Additional Board Manager URLs_ kopieren. 
Anschließend unter _Tools_ -> _Board:_ -> _Boards Manager..._ nach `esp8266` suchen und das Paket installieren. 
  
Abhängigkeiten (Sketch -> Include Library -> Manage Libraries...) :
- DHT Sensor Library by Adafruit

## Installation
Die folgende Anleitung geht davon aus man sieht den Desktop des Respberry Pis.

2. Terminal öffnen und in den _home_ Ordner wechseln. Man kann das Projekt auch woanders ablegen, aber dann müssen später die Pfade in der Datei `usbMqtt.service` geändert werden. 
3. `git clone https://git.rwth-aachen.de/leander.schulten/dht-esp-raspberry-pi-mqtt.git` oder `git clone git@git.rwth-aachen.de:leander.schulten/dht-esp-raspberry-pi-mqtt.git` (Wenn man den Zugriff auf das RWTH gitlab noch nicht eingerichtet hat, siehe [hier](https://git.rwth-aachen.de/symposion/barcodescannerproxy#installation-auf-einem-pi) Punkt 4)
4. `cd dht-esp-raspberry-pi-mqtt`
5. `nano usb_mqtt.py` and change line 6 `MQTT_TOPIC = "test"` to somethink like `MQTT_TOPIC = "<your name>/<room name>"`
6. Abhängigkeiten installieren:
   1. `pip3 install paho-mqtt`
   2. `pip3 install pyserial`
7. Wenn man aktuell nicht im Order `/home/pi/dht-esp-raspberry-pi-mqtt` ist (kann man mit `pwd` prüfen), dann müssen die Pfade in `usbMqtt.service` angepasst werden (`ExecStart` und `WorkingDirectory`)
8. `sudo cp usbMqtt.service /etc/systemd/system/` Den systemd service in das richtige Verzeichnis schieben
9. `sudo systemctl enable usbMqtt` start the service when boot
10. `sudo systemctl start usbMqtt` start the service
11. Check with `systemctl status usbMqtt` and `journalctl -e -u usbMqtt` if everthing works

You can start the service with: `systemctl start usbMqtt`  
You can stop the service with: `systemctl stop usbMqtt`  
Check the status of the service with: `systemctl status usbMqtt`  
You can restart the service with: `systemctl restart usbMqtt`  
  
You can view the output of the server with: `journalctl -e -u usbMqtt`  
You can view the live output of the server with: `journalctl -f -u usbMqtt` 


## MQTT & Zabbix & Grafana
Die Werte werden an MQTT gesendet (topics stehen im Python Quelltext).
Wie [hier](https://git.rwth-aachen.de/hilton-dorm/network-documentation/-/blob/master/services/zabbix.md) beschrieben werden die Daten dann an Zabbix gesendet. 
Dort muss ein Host, [Items](https://verwaltung.hilton.rwth-aachen.de/zabbix/items.php?filter_set=1&filter_hostids%5B0%5D=10326) und [Triggers](https://verwaltung.hilton.rwth-aachen.de/zabbix/triggers.php?filter_set=1&filter_hostids%5B0%5D=10326) erstellt werden.
In Grafana muss ein [Dashboard](https://verwaltung.hilton.rwth-aachen.de/grafana/d/NBpIJJRGk/server-raum-temperature) erstellt werden.