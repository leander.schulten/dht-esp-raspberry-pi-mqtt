#include <DHT.h>

const auto DHTPIN = 2;
const auto DHTTYPE = DHT11;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  Serial.begin(57600);
  dht.begin();
}

void loop() {
  digitalWrite(LED_BUILTIN,HIGH);
 
  Serial.print("humidity:");
  Serial.println(dht.readHumidity());
  Serial.print("temperature:");
  Serial.println(dht.readTemperature());
  
  digitalWrite(LED_BUILTIN,LOW);  
  delay(5000); 
}
